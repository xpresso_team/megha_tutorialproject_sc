import json
from datetime import datetime
import mysql.connector as mysql
import os
cur_work_dir = os.getcwd()
class SampleService():

    MYSQL_IP = "mysql_host"
    MYSQL_PORT = "mysql_port"
    MYSQL_USER = "mysql_user"
    MYSQL_PWD = "mysql_password"
    MYSQL_DB = "mysql_database"

    def __init__(self):

        with open(os.path.abspath(os.path.join(cur_work_dir,"./config/dev.json"))) as f:
            self.config = json.load(f)

        self.db = mysql.connect(host=self.config[self.MYSQL_IP],
                                user=self.config[self.MYSQL_USER],
                                port = self.config[self.MYSQL_PORT],
                                passwd=self.config[self.MYSQL_PWD],
                                database=self.config[self.MYSQL_DB] )
        self.cursor = self.db.cursor()

    def retrieve(self,start,end):
        start = datetime.strptime(start,'%Y-%m-%d')
        end   = datetime.strptime(end,'%Y-%m-%d')
        query = f'SELECT * FROM data WHERE DATE(ts) BETWEEN DATE(\"{start}\") ' \
            f'AND DATE(\"{end}\");'
        print(query)
        self.cursor.execute(query)
        results= self.cursor.fetchall()
        resp = []
        for row in results:
            resp.append({"id":row[0],"message": row[2]})
        return resp


if __name__=="__main__":
    ss = SampleService()
    ss.retrieve("2017-2-20","2029-05-20")
